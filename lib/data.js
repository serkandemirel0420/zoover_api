var fs = require('fs');
var _ = require('lodash');
var moment = require("moment");

function readData(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path,"utf8", function(err, data){
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}

//calculate weigthed ratings data for each rating and aspects
function calculateWeightRatings(data) {
    return _.map(data, function(obj) {

        var result = calculateWeight(obj);

        var clone = _.cloneDeep(result);
        //TODO add weight calculations here as single line
        var calc_ratings = {}
        for (var i in clone.ratings) {
            calc_ratings[i] = {}
            for (var j in clone.ratings[i]) {
                calc_ratings[i][j] = clone.ratings[i][j] * clone.weight
            }
        }
        clone.calculate_ratings = calc_ratings;
         return clone;
    });
}

//calculate weight value
function calculateWeight(data) {
        var ageDifMs = Date.now() -  data.entryDate;
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        let difference =  Math.abs(ageDate.getUTCFullYear() - 1970);
        var weight = null;

        if(difference > 5){
            weight = 0.5;
        }else{
            weight =  (1 - (difference)*0.1)
        }
        data['weight'] = weight;
        return data;
}

//calculate average over weight value
function avg(data,sumWeight) {
    var _this = this;
    return _.mergeWith({}, ...data, function(a, b, c){
        if(_.isNumber(b)) {
            return ((b || 0) / sumWeight) + (_.isNumber(a) ? (a || 0) : 0);
        }
    })
}



module.exports = {
    readData : readData,
    avg : avg,
    calculateWeight : calculateWeight,
    calculateWeightRatings: calculateWeightRatings
}