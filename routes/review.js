var express = require('express');
var router = express.Router();
//utulity lib for reading data
var dataUtil = require('./../lib/data');
var path = require('path');
var _ = require("lodash");


/* GET users listing. */
router.get('/', async function(req, res, next) {


    //relative path of the data file
    let relPath = '../data/review.json';
    //full path resolve
    let fullPath = path.resolve(__dirname, relPath);

    try{
        debugger;

        //Async data load from json file
        var data = await dataUtil.readData(fullPath);
        data = JSON.parse(data);

        //calculate weighted rating data
        var weightedRatingData = dataUtil.calculateWeightRatings(data);

        //extract unique traveledWith values for dropdown
        var  uniqueTravelWidth = _.uniq(_.map(weightedRatingData, 'traveledWith')).map(function(e){ return { label: e, value:e };})


        var offset = Number(req.query.offset);
        var traveledWith = req.query.traveledWith;
        var orderBy = req.query.orderBy;
        var orderType = req.query.orderType;


        //check offset
        if(isNaN(Number(offset)))
             offset = 0;

        //filter by traveledWith value
        if(traveledWith){
            weightedRatingData =_.filter(weightedRatingData, {traveledWith: traveledWith});
        }

        //calculate sum fo weighted values for average calculation
        var sumWeight = _.sum(_.map(weightedRatingData, function(e){ return e.weight }))
        weightedRatingData["sumOfWeight"] = sumWeight;


        //order by requested query
        if(orderBy == "travelDate" || orderBy == "entryDate"  ){
            if(orderType != "asc" && orderType != "desc" ){
                orderType=null;
            }
            weightedRatingData = _.orderBy(weightedRatingData, [orderBy], [(orderType || "asc")]);
        }


        //extract ten of them
        var tenWeightedRatingData = _.take(_.drop(weightedRatingData, offset), 10);

        var filteredData = weightedRatingData.map(function(arr){
            return arr.calculate_ratings;
        });

        var avg = dataUtil.avg(filteredData,sumWeight);


    }catch (err){
        //handle the error through the middleware  - TODO: impl. logging
        return next(err);
    }
    var length = filteredData.length;

    var result = _.merge({
        "weightedRatingData" :tenWeightedRatingData,
        "avg": avg,
        "length" : length,
        "uniqueTravelWidth": uniqueTravelWidth});

    return res.send(result);

});

module.exports = router;
